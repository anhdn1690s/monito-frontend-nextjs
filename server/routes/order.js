const router = require("express").Router();
const ctrls = require("../controllers/order");
const { verifyAccessToken, isAdmin } = require("../middlewares/verifyToken");

router.post("/", [verifyAccessToken], ctrls.createNewOrder);
router.put("/status/:oid", [verifyAccessToken, isAdmin], ctrls.updateStatus);
router.get("/", [verifyAccessToken], ctrls.getOrderUser);
router.get("/admin", [verifyAccessToken, isAdmin], ctrls.getOrderAdmin);

module.exports = router;
