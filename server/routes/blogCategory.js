const router = require("express").Router();
const ctrls = require("../controllers/blogCategory");
const { verifyAccessToken, isAdmin } = require("../middlewares/verifyToken");

router.post("/", [verifyAccessToken, isAdmin], ctrls.createCategoryBlog);
router.get("/", ctrls.getCategoryBlog);
router.put("/:bcid", [verifyAccessToken, isAdmin], ctrls.updateCategoryBlog);
router.delete("/:bcid", [verifyAccessToken, isAdmin], ctrls.deleteCategoryBlog);

module.exports = router;
