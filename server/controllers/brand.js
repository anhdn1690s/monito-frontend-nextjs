const Brand = require("../models/brand");
const asyncHandler = require("express-async-handler");

const createNewBrand = asyncHandler(async (req, res) => {
  const response = await Brand.create(req.body);
  return res.json({
    success: response ? true : false,
    createCategory: response ? response : "Cannot Create new brand",
  });
});

const getBrand = asyncHandler(async (req, res) => {
  const response = await Brand.find()
  return res.json({
    success: response ? true : false,
    brand: response ? response : "Cannot Get brand",
  });
});

const updateBrand = asyncHandler(async (req, res) => {
  const { bid } = req.params;
  const response = await Brand.findByIdAndUpdate(bid, req.body, {
    new: true,
  });
  return res.json({
    success: response ? true : false,
    updateBrand: response ? response : "Cannot Update brand",
  });
});

const deleteBrand = asyncHandler(async (req, res) => {
  const { bid } = req.params;
  const response = await Brand.findByIdAndDelete(bid);
  return res.json({
    success: response ? true : false,
    deleteBrand: response ? response : "Cannot Delete brand",
  });
});

module.exports = {
  createNewBrand,
  getBrand,
  updateBrand,
  deleteBrand,
};
