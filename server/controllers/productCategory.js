const productCategory = require("../models/productCategory");
const asyncHandler = require("express-async-handler");

const createCategory = asyncHandler(async (req, res) => {
  const response = await productCategory.create(req.body);
  return res.json({
    success: response ? true : false,
    createCategory: response ? response : "Cannot Create new product-category",
  });
});

const getCategories = asyncHandler(async (req, res) => {
  const response = await productCategory.find();
  return res.json({
    success: response ? true : false,
    productCategory: response ? response : "Cannot Get product-category",
  });
});

const updateCategories = asyncHandler(async (req, res) => {
  const { pcid } = req.params;
  const response = await productCategory.findByIdAndUpdate(pcid, req.body, {
    new: true,
  });
  return res.json({
    success: response ? true : false,
    updateCategory: response ? response : "Cannot Update product-category",
  });
});

const deleteCategories = asyncHandler(async (req, res) => {
  const { pcid } = req.params;
  const response = await productCategory.findByIdAndDelete(pcid);
  return res.json({
    success: response ? true : false,
    deleteCategory: response ? response : "Cannot Product product-category",
  });
});

module.exports = {
  createCategory,
  getCategories,
  updateCategories,
  deleteCategories,
};
