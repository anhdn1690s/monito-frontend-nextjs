import { ThemeType } from 'grommet'
import Head from 'next/head';
import React from 'react';
import Footer from '../footer'
import Header from '../header'
import classes from './layout.module.scss'
import { ToastContainer } from 'react-toastify'
import ScrollToTop from '../scrollToTop/'

export const globalGrommetTheme: ThemeType = {
  global: {
    font: {
      family: '--curnon-font: Montserrat, -apple-system, BlinkMacSystemFont, sans-serif',
      size: '16px',
    },
  },
}


export default function Layout({ title, children }) {
  return (
    <>
      <Head>
        <title>{title ? title + ' - Monito Pets' : 'Monito Pets'}</title>
        <meta name="description" content="Ecommerce Website" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <div className={classes.root}>
        <div className={classes.mainRoot}>
          <Header />
          <main>{children}</main>
          <Footer />
          <ScrollToTop></ScrollToTop>
        </div>
        <ToastContainer />
      </div>
    </>
  );
}