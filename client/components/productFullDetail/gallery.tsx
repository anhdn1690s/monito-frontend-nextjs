import React, { useEffect, useState } from 'react'
import Image from 'next/image';
import Slider, { Settings } from 'react-slick'
import classes from './gallery.module.scss'




const Gallery = ({ gallery }) => {

    const ArrowRight = (props) => {
        const { onClick } = props

        return (
            <div className={classes.right} onClick={onClick}>
            </div>
        )
    }
    const ArrowLeft = (props) => {
        const { onClick } = props

        return (
            <div className={classes.left} onClick={onClick}>
            </div>
        )
    }

    const [nav1, setNav1] = useState(null)
    const [nav2, setNav2] = useState(null)
    const [slider1, setSlider1] = useState(null)
    const [slider2, setSlider2] = useState(null)


    useEffect(() => {
        setNav1(slider1)
        setNav2(slider2)
    }, [slider1, slider2])

    const settingsMain: Settings = {
        className: 'galleryNav',
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        dots: false,
        nextArrow: <ArrowRight />,
        prevArrow: <ArrowLeft />,
    }

    const settingsThumbs: Settings = {
        className: 'galleryThumbs',
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        swipeToSlide: true,
        focusOnSelect: true,
        infinite: false,
        vertical: true,
        verticalSwiping: true,
    }
    return (
        <div className={classes.root}>
            <div className={classes.sliderThumbs}>
                <Slider {...settingsMain} asNavFor={nav2} ref={(slider) => setSlider1(slider)}>
                    {gallery.imageDetail.map((imgFile, index) => {
                        return (
                            <div className={classes.boxImgThumbs} key={index}>
                                <Image
                                    placeholder="blur"
                                    src={imgFile}
                                    alt="image"
                                    width={560}
                                    height={560}
                                    className={classes.img}
                                />
                            </div>
                        )
                    })}
                </Slider>
            </div>
            <div className={classes.sliderNav}>
                <Slider {...settingsThumbs} asNavFor={nav1} ref={(slider) => setSlider2(slider)}>
                    {gallery.imageDetail.map((imgFile, index) => {
                        return (
                            <div className={classes.boxImgThumbs} key={index}>
                                <Image
                                    className={classes.imgNav}
                                    placeholder="blur"
                                    src={imgFile}
                                    alt="image"
                                    width={95}
                                    height={95}
                                />
                            </div>
                        )
                    })}
                </Slider>
            </div>
        </div>
    )
}

export default Gallery