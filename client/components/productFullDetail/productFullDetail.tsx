import classes from './productFullDetail.module.scss'
import React from 'react'
import Gallery from './gallery'
import { formatPrice } from '../../utils/formatPrice'
import messIcon from '../../assets/image/mess.svg'
import shareIcon from '../../assets/image/share.svg'
import catIcon from '../../assets/image/cat.svg'
import dogIcon from '../../assets/image/dog.svg'
import Skeleton from 'react-loading-skeleton'

const ProductFullDetail = ({ product }) => {
    return (
        <div className={classes.root}>
            <div className={classes.rootMain}>
                <div className={classes.boxImage}>
                    <Gallery gallery={product} />
                </div>
                <div className={classes.boxInfo}>
                    <p className={classes.sku}>SKU #1000078</p>
                    <p className={classes.name}>{product.name}</p>
                    <p className={classes.price}>                        {
                        formatPrice(product.price) ?? <Skeleton width={80} height={15} ></Skeleton>
                    }</p>
                    <div className={classes.boxButton}>
                        <button className={classes.buttonContact}>
                            Contact us
                        </button>
                        <button className={classes.buttonMess}>
                            <span><img src={messIcon.src} alt="Img" /></span> Chat with Monito
                        </button>
                    </div>
                    <div className={classes.boxShare}>
                        <p>
                            Information
                        </p>
                        <p><span><img src={shareIcon.src} alt="Img" /></span>Share</p>
                    </div>
                    <div className={classes.boxAttribute}>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>SKU</p>
                            <p className={classes.value}>: #1000078</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Gender</p>
                            <p className={classes.value}>: Female</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Age</p>
                            <p className={classes.value}>: 2 Months</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Size</p>
                            <p className={classes.value}>: Small</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Color</p>
                            <p className={classes.value}>: Appricot & Tan</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Vaccinated</p>
                            <p className={classes.value}>: Yes</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Dewormed</p>
                            <p className={classes.value}>: Yes</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Cert</p>
                            <p className={classes.value}>: Yes (MKA)</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Microchip</p>
                            <p className={classes.value}>: Yes</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Location</p>
                            <p className={classes.value}>: Vietnam</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Published Date</p>
                            <p className={classes.value}>: 12-Oct-2022</p>
                        </div>
                        <div className={classes.boxItem}>
                            <p className={classes.label}>Additional Information</p>
                            <p className={classes.value}>: Pure breed Shih Tzu.
                                Good body structure.
                                With MKA cert and microchip. Father from champion lineage.</p>
                        </div>
                    </div>
                    <div className={classes.boxHealth}>
                        <div className={classes.itemHealth}>
                            <img src={dogIcon.src} alt="Img" />
                            <p>100% health guarantee for pets</p>
                        </div>
                        <div className={classes.itemHealth}>
                            <img src={catIcon.src} alt="Img" />
                            <p>100% guarantee of pet identification</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ProductFullDetail