import { FC, PropsWithChildren, useEffect, useRef, useState } from 'react'
import { createPortal } from 'react-dom'

interface IModal {
  container?: HTMLElement
  selector?: string
}

const Modal: FC<PropsWithChildren<IModal>> = ({ children, selector = '#modal' }) => {
  const ref = useRef()
  const [mounted, setMounted] = useState(false)

  useEffect(() => {
    ref.current = document.querySelector(selector)
    setMounted(true)
  }, [selector])
  return mounted ? createPortal(children, ref.current) : null
}

export default Modal
