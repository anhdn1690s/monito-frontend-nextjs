import classes from "./productCard.module.scss";
import Link from "next/link";
import Skeleton from "react-loading-skeleton";
import Img from "next/image";
import { formatPrice } from "../../utils/formatPrice";

export const ProductCard = ({ product }) => {
  return (
    <div className={classes.root}>
      <div className={classes.rootMain}>
        <div className={classes.item}>
          <Link href={`/product/${product.slug}`}>
            <a>
              {<img src={product.thumb} className={classes.productImage} /> ?? (
                // <Img
                //   className={classes.productImage}
                //   src={product.thumb}
                //   alt="product"
                //   layout="responsive"
                //   loading="lazy"
                //   width={400}
                //   height={400}
                // />
                <Skeleton width="100%" height="169px"></Skeleton>
              )}
            </a>
          </Link>
          <div className={classes.info}>
            <Link href="/">
              <a className={classes.name}>
                {product.title ?? <Skeleton width={100} height={15}></Skeleton>}
              </a>
            </Link>
            <p className={classes.text}>
              {(
                <>
                  Gene: <span> {product.gender} </span>
                </>
              ) ?? <Skeleton width={70} height={15}></Skeleton>}
            </p>
            <p className={classes.text}>
              {(
                <>
                  Age: <span>{product.age}</span>
                </>
              ) ?? <Skeleton width={70} height={15}></Skeleton>}
            </p>
            <p className={classes.price}>
              {formatPrice(product.price) ?? (
                <Skeleton width={80} height={15}></Skeleton>
              )}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};
