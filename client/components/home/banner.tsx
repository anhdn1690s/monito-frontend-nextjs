import React from 'react'
import { motion } from 'framer-motion'
import Link from 'next/link'
import classes from './banner.module.scss'
import playIcon from '../../assets/image/play.svg'
import dogImg from '../../assets/image/dog-edit.png'
import dogMobiImg from '../../assets/image/dogMobi.png'

const textAnimate = {
    offscreen: { y: 20, opacity: 0 },
    onscreen: {
        y: 0,
        opacity: 1,
        transition: {
            type: "spring",
            bounce: 0,
            duration: 0.6
        }
    }

}
const buttonAnimate1 = {
    offscreen: { x: 20, opacity: 0 },
    onscreen: {
        x: 0,
        opacity: 1,
        transition: {
            type: "spring",
            bounce: 0,
            duration: 0.6
        }
    }
}
const buttonAnimate2 = {
    offscreen: { x: -20, opacity: 0 },
    onscreen: {
        x: 0,
        opacity: 1,
        transition: {
            type: "spring",
            bounce: 0,
            duration: 0.6
        }
    }
}


let easeing = [0.6, -0.05, 0.01, 0.99];


const imgAnimate = {
    initial: {
        x: 60,
        opacity: 0,
        transition: { duration: 0.05, ease: easeing }
    },
    animate: {
        x: 0,
        opacity: 1,
        animation: {
            duration: 0.6,
            ease: easeing
        }
    }
};




export const Banner = () => {
    return (
        <div className={classes.root}>
            <div className={classes.rootMain} >
                <motion.div className={classes.contentText} initial={"offscreen"}
                    whileInView={"onscreen"}
                    viewport={{ once: true, amount: 0.5 }}
                    transition={{ staggerChildren: 0.5 }}>
                    <motion.h1 className={classes.title} variants={textAnimate}>One more friend</motion.h1>
                    <motion.h3 variants={textAnimate}>Thousands more fun!</motion.h3>
                    <motion.p variants={textAnimate}>Having a pet means you have more joy, a new friend, a happy person who will always be with you to have fun. We have 200+ different pets that can meet your needs!</motion.p>
                    <div className={classes.boxButton}>
                        <motion.div className={classes.buttonView} variants={buttonAnimate1}>
                            <Link href="/">
                                <a>
                                    View Intro
                                    <span><img src={playIcon.src} alt="Img" /></span>
                                </a>
                            </Link>
                        </motion.div>
                        <motion.div className={classes.buttonExplore} variants={buttonAnimate2}>
                            <Link href="/">
                                <a>
                                    Explore Now
                                </a>
                            </Link>
                        </motion.div>
                    </div>
                </motion.div>
                <motion.div className={classes.img} initial='initial' animate='animate'>
                    <picture >
                        <source srcSet={dogImg.src} media="(min-width:1024px)"></source>
                        <motion.img src={dogMobiImg.src} alt="img" variants={imgAnimate}/>
                    </picture>
                </motion.div>
            </div>
        </div>
    )
}