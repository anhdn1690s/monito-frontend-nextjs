import React, { useEffect, useState } from "react";
import Link from "next/link";
import classes from "./bestSellers.module.scss";
import { ProductCard } from "../productCard/productCard";
import bottomIcon from "../../assets/image/bottom.svg";
import { motion } from "framer-motion";
import { apiGetProducts } from "../../pages/api/product";

const container = {
  visible: {
    transition: {
      delayChildren: 0.2,
      staggerChildren: 0.2,
    },
  },
};

const item = {
  hidden: { y: 20, opacity: 0 },
  visible: {
    y: 0,
    opacity: 1,
    transition: {
      ease: "easeInOut",
      duration: 0.5,
    },
  },
};

const BestSellers = () => {
  const [bestSellers, setBestSellers] = useState(null);

  useEffect(() => {
    const fetchProduct = async () => {
      const response = await apiGetProducts();
      if (response?.data?.success) {
        setBestSellers(response?.data?.products);
      }
    };

    fetchProduct();
  }, []);

  console.log("data", bestSellers);

  return (
    <div className={classes.root}>
      <div className={classes.contentHeader}>
        <div className={classes.text}>
          <p className={classes.whats}>Whats new?</p>
          <p className={classes.sake}>Take a look at some of our pets</p>
        </div>
        <div className={classes.buttonDesktop}>
          <Link href="/">
            <a>
              View more{" "}
              <span>
                <img src={bottomIcon.src} alt="Img" />
              </span>
            </a>
          </Link>
        </div>
      </div>
      <motion.div
        className={classes.rootMain}
        variants={container}
        initial="hidden"
        exit="exit"
        whileInView="visible"
        viewport={{ once: true }}
      >
        {bestSellers &&
          bestSellers.map((product, index) => (
            <motion.div variants={item} key={index}>
              <ProductCard product={product} />
            </motion.div>
          ))}
      </motion.div>
      <div className={classes.buttonMobi}>
        <Link href="/">
          <a>
            View more{" "}
            <span>
              <img src={bottomIcon.src} alt="Img" />
            </span>
          </a>
        </Link>
      </div>
    </div>
  );
};
export default BestSellers;
