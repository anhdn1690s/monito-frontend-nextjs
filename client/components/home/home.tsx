// import FacebookImage from 'assets/images/global/facebook.jpg'
import { NextSeo } from 'next-seo'
import { FunctionComponent } from 'react'
import classes from './home.module.scss'
import { Banner } from './banner'
import BestSellers from './bestSellers'
import LineBanner from './lineBanner'
interface IComponentProps { }

export const Home: FunctionComponent<IComponentProps> = () => {
  return (
    <>
      <NextSeo
        title={`Monito Pets for Best | Monito`}
        description="One more friend Thousands more fun!"
      // openGraph={{
      //   images: [
      //     {
      //       width: 3000,
      //       height: 1575,
      //       url: 'https://curnonwatch.com' + FacebookImage.src,
      //     },
      //   ],
      // }}
      />
      <div className={classes.root}>
        <Banner />
        <BestSellers />
        <LineBanner />
      </div>
    </>
  )
}
