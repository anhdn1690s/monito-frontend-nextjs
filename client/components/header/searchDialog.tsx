import { motion } from 'framer-motion'
import React, { useCallback } from 'react'
import { useHeaderState } from '../../hooks/stores/useHeaderState'
import Mask from '../Mask'
import Modal from '../Modal'
import { ProductCard } from '../productCard/productCard'
import classes from './searchDialog.module.scss'

const SearchDialog = () => {

    const searchDialogDrawer = useHeaderState(
        (state) => state.searchDialogDrawer
    );
    const setSearchDialogDrawer = useHeaderState(
        (state) => state.setSearchDialogDrawer
    );
    const closeModal = useCallback(
        () => setSearchDialogDrawer(false),
        [searchDialogDrawer]
    );

    return (
        <Modal>
            <motion.div
                initial={{ translateY: '-100vw', opacity: 0 }}
                variants={{
                    open: {
                        translateY: '-50%',
                        opacity: 1,
                        zIndex: 20,
                    },
                    closed: {
                        translateY: '-100vw',
                        opacity: 0,
                        zIndex: -1,
                    },
                }}
                transition={{ ease: 'easeIn' }}
                className={classes.root}
                animate={searchDialogDrawer ? 'open' : 'closed'}

            >
                <div className={classes.body}>
                    <div className={classes.close} onClick={closeModal}>X</div>
                    <motion.div className={classes.header} layout>
                        <div className={classes.searchForm}>
                            <input
                                type="search"
                                name="search"
                                placeholder="Nhập từ khóa..."
                                // onChange={}
                                className={classes.searchInput}
                            // value={}
                            ></input>
                        </div>
                    </motion.div>
                    <motion.div layout>
                        <div className={classes.productGridWrapper}>
                            {/* <div className={classes.productGrid}>
                                {hookProps.products?.map((product) => {
                                    return (
                                        <ProductCard
                                            product={product}
                                            key={product?.id}
                                        ></ProductCard>
                                    )
                                })}
                            </div> */}

                        </div>
                    </motion.div>
                </div>
            </motion.div>

            <Mask
                isActive={searchDialogDrawer}
                classes={{
                    root_active: classes.maskRootActive,
                    root: classes.maskRoot,
                }}
                dismiss={() => setSearchDialogDrawer(false)}
            />

        </Modal>
    )
}

export default SearchDialog