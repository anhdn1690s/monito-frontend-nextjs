import React from 'react'
import classes from './header.module.scss'
import Link from 'next/link'
import { MenuMobile } from './menuMobile'
import { motion } from 'framer-motion';
import { useHeaderState } from "../../hooks/stores/useHeaderState";
import logoIcon from '../../assets/image/logo.png'
import searchIcon from '../../assets/image/search.svg'
import menuIcon from '../../assets/image/menu.svg'
import searchIconMobi from '../../assets/image/sreach-mobi.svg'
import SearchDialog from './searchDialog';


let easeing = [0.6, -0.05, 0.01, 0.99];


const header = {
  initial: {
    y: -60,
    opacity: 0,
    transition: { duration: 0.05, ease: easeing }
  },
  animate: {
    y: 0,
    opacity: 1,
    animation: {
      duration: 0.6,
      ease: easeing
    }
  }
};


const Header = () => {

  const setMobileLeftDrawer = useHeaderState(
    (state) => state.setMobileLeftDrawer
  );

  const setSearchDialogDrawer = useHeaderState(
    (state) => state.setSearchDialogDrawer
  );


  return (
    <motion.div className={classes.root} initial='initial' animate='animate'>
      <div className={classes.mainRoot}>
        <div className={classes.menuLeft}>
          <div className={classes.menuIcon} onClick={() => {
            setMobileLeftDrawer(true);
          }}

          >
            <img src={menuIcon.src} alt="Img" />
          </div>
          <motion.div className={classes.logo} variants={header}>
            <Link href="/">
              <a>
                <img src={logoIcon.src} className={classes.logo} alt="logo" />
              </a>
            </Link>
          </motion.div>
          <div className={classes.searchIcon} onClick={() => {
            setSearchDialogDrawer(true);
          }}>
            <img src={searchIconMobi.src} alt="Img" />
          </div>
          <nav className={classes.navDesktop}>
            <ul>
              <li>
                <Link href="/">
                  <a>
                    Home
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    Category
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    About
                  </a>
                </Link>
              </li>
              <li>
                <Link href="/">
                  <a>
                    Contact
                  </a>
                </Link>
              </li>
            </ul>
          </nav>
        </div>
        <div className={classes.menuRight}>
          <div className={classes.searchDialog} onClick={() => {
            setSearchDialogDrawer(true);
          }}>
            <div className={classes.search}>
              <img src={searchIcon.src} alt="Img" />
            </div>
          </div>
          <div className={classes.community}>
            <Link href="/">
              <a>
                Join the community
              </a>
            </Link>
          </div>
        </div>
      </div>
      <MenuMobile key="mobile-menu" />
      <SearchDialog key="search-dialog" />
    </motion.div>
  )
}
export default Header
