import React, { useState, useEffect } from 'react'
import classes from './scrollToTop.module.scss'

const ScrollToTop = (props) => {
  const [showTopBtn, setShowTopBtn] = useState(false)
  useEffect(() => {
    window.addEventListener('scroll', () => {
      if (window.scrollY > 400) {
        setShowTopBtn(true)
      } else {
        setShowTopBtn(false)
      }
    })
  }, [])
  const goToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }
  return (
    <div className={classes.root}>
      <div className={classes.topToBtm}>
        {showTopBtn && (
          <span className={classes.iconPosition} onClick={goToTop}>
            <svg
              width="8"
              height="12"
              viewBox="0 0 8 12"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M0.806569 1.36929L5.5073 6L0.80657 10.6307C0.397811 10.9959 0.397811 11.361 0.80657 11.7261C1.18127 12.0913 1.55596 12.0913 1.93066 11.7261L7.24453 6.54772C7.41484 6.41494 7.5 6.23237 7.5 6C7.5 5.76763 7.41484 5.58506 7.24453 5.45228L1.93066 0.273859C1.55596 -0.0912863 1.18127 -0.0912863 0.806569 0.273859C0.39781 0.639004 0.39781 1.00415 0.806569 1.36929Z"
                fill="#F8F7F4"
              />
            </svg>
          </span>
        )}
      </div>
    </div>
  )
}
export default ScrollToTop
