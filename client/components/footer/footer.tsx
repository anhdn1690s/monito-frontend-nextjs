import Link from 'next/link'
import React from 'react'
import classes from './footer.module.scss'
import iconFb from '../../assets/image/icon-fb.svg'
import iconTw from '../../assets/image/icon-tw.svg'
import iconIg from '../../assets/image/icon-ig.svg'
import iconYtb from '../../assets/image/icon-ytb.svg'
import logoIcon from '../../assets/image/logo.png'

const Footer = () => {
  const copyrightDate = new Date();
  return (
    <div className={classes.root}>
      <div className={classes.mainRoot}>
        <div className={classes.formBox}>
          <h2>Register now so you don't miss our programs</h2>
          <form>
            <input type="text" placeholder='Enter your Email' name='text' />
            <button type='button'>
              Subcribe Now
            </button>
          </form>
        </div>
        <div className={classes.cateMenu}>
          <div className={classes.menu}>
            <Link href={"/"}>
              <a href="">
                Home
              </a>
            </Link>
            <Link href={"/"}>
              <a href="">
                Category
              </a>
            </Link>
            <Link href={"/"}>
              <a href="">
                About
              </a>
            </Link>
            <Link href={"/"}>
              <a href="">
                Contact
              </a>
            </Link>
          </div>
          <div className={classes.icon}>
            <img src={iconFb.src} alt="Img" />
            <img src={iconTw.src} alt="Img" />
            <img src={iconIg.src} alt="Img" />
            <img src={iconYtb.src} alt="Img" />
          </div>
        </div>
        <div className={classes.logoAllRight}>
          <p className={classes.textDesktop}>© {copyrightDate.getFullYear()} Monito. All rights reserved.</p>
          <img src={logoIcon.src} alt="Img" />
          <ul>
            <Link href={"/"}>
              Terms of Service
            </Link>
            <Link href={"/"}>
              Privacy Policy
            </Link>
          </ul>
          <p className={classes.textMobile}>© {copyrightDate.getFullYear()} Monito. All rights reserved.</p>
        </div>
      </div>
    </div>
  )
}

export default Footer