import { AnimatePresence, motion } from 'framer-motion'
import { useRouter } from 'next/router'
import '../styles/globals.scss'

function MyApp({ Component, pageProps }) {

  const router = useRouter()
  return (

    <AnimatePresence>
      <motion.div
        key={router.route}
        initial="initialSate"
        animate="animateSate"
        exit="exitSate"
        transition={{
          duration: 0.8,
        }}
        variants={
          {
            initialSate: {
              opacity: 0,
            },
            animateSate: {
              opacity: 1,
            },
          }
        }
      >
        <Component {...pageProps} />
      </motion.div>
    </AnimatePresence>
  )
}

export default MyApp