
import { useRouter } from 'next/router';
import React from 'react';
import Layout from '../../components/layout/layout';
import data from '../../utils/data';
import Error from 'next/error'
import ProductFullDetail from '../../components/productFullDetail';

const Product = () => {
    const { query } = useRouter();
    const { slug } = query;
    const product = data.products.find((x) => x.slug === slug);
    if (!product) {
        return <Error statusCode={404} title="Non-existent product!" />;
    }
    return (
        <Layout title={product.name}>
            <ProductFullDetail product={product} />
        </Layout >
    )
}

export default Product