import axios from "../../utils/axios";

export const apiGetProducts = () =>
  axios({
    url: "/product/",
    method: "get",
  });
export const apiProducts = (pid) =>
  axios({
    url: "/product/" + pid,
    method: "get",
  });
