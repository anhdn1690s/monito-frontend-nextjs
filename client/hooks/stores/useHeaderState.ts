import produce from "immer"; //without immer it wont trigger update
import create from "zustand";

interface HeaderStateStore {
  miniCartDrawer: boolean;
  setMiniCartDrawer: (val: boolean) => void;
  mobileLeftDrawer: boolean;
  setMobileLeftDrawer: (val: boolean) => void;
  crossSellDrawer: boolean;
  setCrossSellDrawer: (val: boolean) => void;
  searchDialogDrawer: boolean;
  setSearchDialogDrawer: (val: boolean) => void;
}
export const useHeaderState = create<HeaderStateStore>((set) => ({
  miniCartDrawer: false,
  mobileLeftDrawer: false,
  crossSellDrawer: false,
  searchDialogDrawer: false,
  setMiniCartDrawer: (val) =>
    set(
      produce((state: HeaderStateStore) => {
        state.miniCartDrawer = val;
      })
    ),
  setMobileLeftDrawer: (val) =>
    set(
      produce((state: HeaderStateStore) => {
        state.mobileLeftDrawer = val;
      })
    ),
  setCrossSellDrawer: (val) =>
    set(
      produce((state: HeaderStateStore) => {
        state.crossSellDrawer = val;
      })
    ),
  setSearchDialogDrawer: (val) =>
    set(
      produce((state: HeaderStateStore) => {
        state.searchDialogDrawer = val;
      })
    ),
}));
