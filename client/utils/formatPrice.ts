export const formatPrice = (value: number, currencyCode: string = 'VND') => {
  const fixedLocale = 'vi-VN'
  return new Intl.NumberFormat(fixedLocale, { style: 'currency', currency: currencyCode }).format(
    value
  )
}
