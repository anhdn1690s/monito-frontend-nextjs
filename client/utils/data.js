import bcrypt from 'bcryptjs';
import pro1 from '../assets/image/pro01.png'
import pro2 from '../assets/image/pro02.png'
import pro3 from '../assets/image/pro03.png'
import pro4 from '../assets/image/pro04.png'
import pro5 from '../assets/image/pro05.png'
import pro6 from '../assets/image/pro06.png'
import pro7 from '../assets/image/pro07.png'
import pro8 from '../assets/image/pro08.png'
import proDetail1 from '../assets/image/image-detail01.jpg'
import proDetail2 from '../assets/image/image-detail02.jpg'
import proDetail3 from '../assets/image/image-detail03.jpg'
import proDetail4 from '../assets/image/image-detail04.jpg'
import proDetail5 from '../assets/image/image-detail05.jpg'


const data = {
    users: [
        {
            name: 'John',
            email: 'admin@example.com',
            password: bcrypt.hashSync('123456'),
            isAdmin: true,
        },
        {
            name: 'Jane',
            email: 'user@example.com',
            password: bcrypt.hashSync('123456'),
            isAdmin: false,
        },
    ],
    products: [
        {
            id: 1,
            name: 'Pomeranian White',
            gene: 'Male',
            age: '02 months',
            slug: 'ppmeranian-white',
            category: 'Shirts',
            image: pro1,
            imageDetail: [proDetail1, proDetail2, proDetail3, proDetail4, proDetail5],
            price: 6900000,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 8,
            countInStock: 20,
            description: 'A popular shirt',
        },
        {
            id: 2,
            name: 'Pomeranian White',
            gene: 'Male',
            age: '02 months',
            slug: 'ppmeranian-white',
            category: 'Shirts',
            image: pro2,
            imageDetail: [proDetail1, proDetail2, proDetail3, proDetail4, proDetail5],
            price: 6900000,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 8,
            countInStock: 20,
            description: 'A popular shirt',
        },
        {
            id: 3,
            name: 'Pomeranian White',
            gene: 'Male',
            age: '02 months',
            slug: 'ppmeranian-white',
            category: 'Shirts',
            image: pro3,
            imageDetail: [proDetail1, proDetail2, proDetail3, proDetail4, proDetail5],
            price: 6900000,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 8,
            countInStock: 20,
            description: 'A popular shirt',
        },

        {
            id: 4,
            name: 'Pomeranian White',
            gene: 'Male',
            age: '02 months',
            slug: 'ppmeranian-white',
            category: 'Shirts',
            image: pro4,
            imageDetail: [proDetail1, proDetail2, proDetail3, proDetail4, proDetail5],
            price: 6900000,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 8,
            countInStock: 20,
            description: 'A popular shirt',
        },
        {
            id: 5,
            name: 'Pomeranian White',
            gene: 'Male',
            age: '02 months',
            slug: 'pomeranian-white',
            category: 'Shirts',
            image: pro5,
            imageDetail: [proDetail1, proDetail2, proDetail3, proDetail4, proDetail5],
            price: 6900000,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 8,
            countInStock: 20,
            description: 'A popular shirt',
        },
        {
            id: 6,
            name: 'Pomeranian White',
            gene: 'Male',
            age: '02 months',
            slug: 'ppmeranian-white',
            category: 'Shirts',
            image: pro6,
            imageDetail: [proDetail1, proDetail2, proDetail3, proDetail4, proDetail5],
            price: 6900000,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 8,
            countInStock: 20,
            description: 'A popular shirt',
        },
        {
            id: 7,
            name: 'Pomeranian White',
            gene: 'Male',
            age: '02 months',
            slug: 'ppmeranian-white',
            category: 'Shirts',
            image: pro7,
            imageDetail: [proDetail1, proDetail2, proDetail3, proDetail4, proDetail5],
            price: 6900000,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 8,
            countInStock: 20,
            description: 'A popular shirt',
        },
        {
            id: 8,
            name: 'Pomeranian White',
            gene: 'Male',
            age: '02 months',
            slug: 'ppmeranian-white',
            category: 'Shirts',
            image: pro8,
            imageDetail: [proDetail1, proDetail2, proDetail3, proDetail4, proDetail5],
            price: 6900000,
            brand: 'Nike',
            rating: 4.5,
            numReviews: 8,
            countInStock: 20,
            description: 'A popular shirt',
        },
    ],
};
export default data;