// import { combineReducers, configureStore } from '@reduxjs/toolkit'
// import {
//   createMigrate,
//   createTransform,
//   FLUSH,
//   PAUSE,
//   PERSIST,
//   persistReducer,
//   persistStore,
//   PURGE,
//   REGISTER,
//   REHYDRATE,
// } from 'redux-persist'
// import sessionStorage from 'redux-persist/lib/storage/session'
// import localStorage from 'redux-persist/lib/storage'
// import createSagaMiddleware from 'redux-saga'
// import { select } from 'redux-saga/effects'
// import { Client } from 'urql'
// import { migrations } from './migrations'
// import RootSaga from './sagas'
// import { analyticsSlice } from './slices/analytics'
// import { appSlice, AppState } from './slices/app'
// import { cartSlice, CartState } from './slices/cart'
// import { categorySlice } from './slices/category'
import { createWrapper } from 'next-redux-wrapper'

// const rootReducer = combineReducers({
//   [appSlice.name]: persistReducer(
//     {
//       key: 'app',
//       storage: sessionStorage,
//       version: 2,
//     },
//     appSlice.reducer
//   ),
//   [categorySlice.name]: categorySlice.reducer,
//   [cartSlice.name]: cartSlice.reducer,
//   [analyticsSlice.name]: analyticsSlice.reducer,
// })

// const cartStateTransformAndBlacklist = createTransform<
//   CartState,
//   CartState,
//   ReturnType<typeof rootReducer>,
//   CartState
// >(
//   null,
//   (state, key) => {
//     const newState: CartState = { ...state }
//     newState.loading = 0
//     newState.needSendToGTM = false
//     newState.needUpdate = true
//     return newState
//   },
//   { whitelist: ['cart'] }
// )
// const appStateTransformAndBlacklist = createTransform<
//   AppState,
//   AppState,
//   ReturnType<typeof rootReducer>,
//   AppState
// >(
//   null,
//   (state, key) => {
//     const newState2: AppState = { ...state }
//     newState2.modal = undefined
//     newState2.isLoading = false
//     return newState2
//   },
//   { whitelist: ['app'] }
// )

// const persistConfig = {
//   key: 'root',
//   version: 2,
//   storage: localStorage,
//   migrate: createMigrate(migrations, { debug: false }),
//   transforms: [cartStateTransformAndBlacklist, appStateTransformAndBlacklist],
//   blacklist: ['app'],
// }

// export const makeStore = () => {
//   let store: any
//   const persistedReducer = persistReducer(persistConfig, rootReducer)
//   const sagaMiddleware = createSagaMiddleware()
//   if (typeof window !== 'undefined') {
//     //client

//     store = configureStore({
//       reducer: persistedReducer,
//       // preloadedState: initialState,
//       middleware: (getDefaultMiddleware) => {
//         const defaultMiddleware = getDefaultMiddleware({
//           serializableCheck: {
//             ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
//           },
//           // thunk: {
//           // extraArgument: { client },
//           // },
//         })
//         return defaultMiddleware.concat(sagaMiddleware)
//       },
//     })
//     ;(store as any).__persistor = persistStore(store)
//   } else {
//     store = configureStore({
//       reducer: rootReducer,
//       // preloadedState: initialState,
//       middleware: (getDefaultMiddleware) => {
//         const defaultMiddleware = getDefaultMiddleware({
//           serializableCheck: {
//             ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
//           },
//           // thunk: {
//           // extraArgument: { client },
//           // },
//         })
//         return defaultMiddleware.concat(sagaMiddleware)
//       },
//     })
//   }
//   sagaMiddleware.run(RootSaga)

//   return store
// }

// // Infer the `RootState` and `AppDispatch` types from the store itself
export type MakeStoreState = ReturnType<typeof makeStore>
// export type RootState = ReturnType<MakeStoreState['getState']>
// export type AppDispatch = MakeStoreState['dispatch']
// export function* appSelect<TSelected>(
//   selector: (state: RootState) => TSelected
// ): Generator<any, TSelected, TSelected> {
//   return yield select(selector)
// }

// export an assembled wrapper
export const wrapper = createWrapper(makeStore, { debug: false })
