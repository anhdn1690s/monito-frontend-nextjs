const path = require('path')
const withPlugins = require('next-compose-plugins')
const withPWA = require('next-pwa')
const withBundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
})

module.exports = withPlugins(
  [
    [withBundleAnalyzer],
    // [
    //   withSentryConfig,
    //   {
    //     silent: true,
    //   },
    // ],
  ],
  {
    compiler: {
      styledComponents: true,
    },
    eslint: {
      // Warning: Dangerously allow production builds to successfully complete even if
      // your project has ESLint errors.
      ignoreDuringBuilds: true,
    },
    webpack5: true,
    images: {

    },
    swcMinify: true,
    webpack: (config, options) => {
      config.resolve.alias = {
        ...config.resolve.alias,
        '~': path.resolve(__dirname),
      }
      config.module.rules.push({
        test: /\.svgm$/,
        use: ['@svgr/webpack', 'url-loader'],
      })

      return config
    },
    trailingSlash: true,
    async rewrites() {
      return {
        beforeFiles: [
          // These rewrites are checked after headers/redirects
          // and before all files including _next/public files which
          // allows overriding page files
          {
            source: '/blog/:path(.*)',
            destination: '/api/blog/:path',
          },
          // {
          //   source: '/store/:pathname*',
          //   destination: process.env.NEXT_PUBLIC_BACKEND_URL + '/:pathname*',
          // },
          {
            source: '/sitemap.xml',
            destination: '/api/sitemap?resource=default',
          },
        ],
      }
    },
    async redirects() {
      return [
        {
          source: '/:slug*.html',
          destination: '/:slug*',
          permanent: true,
        },
        {
          source: '/collections/whitesands',
          destination: '/dong-ho/dong-ho-nam/dong-ho-nam-whitesands',
          permanent: false,
        },
      ]
    },
  }
)
